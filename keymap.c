/*
--Layer 0--
  _________________________________________________________________________________________________
 |[esc]            [1!]-[2@]-[3#]-[4$]-[5%]-[6^]-[7&]-[8*]-[9(]-[0)]-[-_]-[+=]      [  BackSpace  ]|
 |[ Tab ]               [Q]-[W]-[E]-[R]-[T]-[Y]-[U]-[I]-[O]-[P]-[[{]-[]}]                    [ |\ ]|
 |[ To(1) ]              [A]-[S]-[D]-[F]-[G]-[H]-[J]-[K]-[L]-[;/:]-['/"]                    [Enter]|
 |[ Shift ]               [Z]-[X]-[C]-[V]-[B]-[N]-[M]-[</,]-[>/.]-[?//]                     [Shift]|
 |[ctrl][win][alt]        [____________________SPACE__________________]   [alt][Mo(1)][Mo(2)][ctrl]|
 |_________________________________________________________________________________________________|



--Layer 1--
  _______________________________________________________________________________________
 |[`]        [F1]-[F2]-[F3]-[F4]-[F5]-[F6]-[F7]-[F8]-[F9]-[F10]-[F11]-[F12]   [   del   ]|
 |[ ▼ ]    [▼]-[Prv]-[Play]-[Nxt]-[▼]-[▼]-[Calc]-[PS]-[Ins]-[Pse]-[Up]-[▼]    [ RESET ]|
 |[  ▼  ] [V+]-[V-]-[Mute]-[Ejt]-[▼]-[Pst]-[Psls]-[Home]-[PgUp]-[Left]-[Rght]    [  ▼  ]|
 |[   ▼   ]      [▼]-[▼]-[▼]-[▼]-[▼]-[Ppls]-[Pmns]-[End]-[PgDn]-[Down]            [ ▼ ]|
 |[ ▼ ][ ▼ ][ ▼ ]        [_________________▼________________]          [▼][▼][Mo(3)][▼]|
 |_______________________________________________________________________________________|



--Layer 2--
  __________________________________________________________________________________________________
 |[ VLKTgg ]    [KP1]-[KP2]-[KP3]-[KP4]-[KP5]-[KP6]-[KP7]-[KP8]-[KP9]-[KP0]-[KP-]-[KP+]     [ctrl+`]| 
 |[ RGBTgg ]       [Mod+]-[Mod-]-[Hue+]-[Hue-]-[▼]-[▼]-[▼]-[▼]-[▼]-[▼]-[▼]-[KP=]            [ ▼ ]| 
 |[  To(3)  ]        [Spd+]-[Spd-]-[Sat+]-[Sat-]-[▼]-[▼]-[▼]-[▼]-[▼]-[▼]-[KP*]           [KPEnter]| 
 |[   ▼   ]            [▼]-[▼]-[Brt+]-[Brt-]-[▼]-[▼]-[▼]-[KP,]-[KP.]-[KP/]                 [  ▼  ]| 
 |[  ▼  ][ ▼ ][▼]     [_________________________▼_________________________][▼][ To(3) ] [ ▼ ][ ▼ ]| 
 |__________________________________________________________________________________________________|



--Layer 3--
  _________________________________________________________________________________________
 |[▼]        [M1]-[M2]-[M3]-[M4]-[M5]-[M6]-[M7]-[M8]-[M9]-[M10]-[M11]-[M12]     [   del   ]|
 |[ ▼ ]        [▼]-[▼]-[▼]-[▼]-[▼]-[▼]-[PS]-[▼]-[▼]-[▼]-[▼]-[▼]               [  ▼  ]|
 |[   Tg(4)   ]        [▼]-[▼]-[▼]-[▼]-[▼]-[▼]-[▼]-[▼]-[▼]-[▼]-[▼]              [  ▼  ]|
 |[   ▼   ]            [▼]-[▼]-[▼]-[▼]-[▼]-[▼]-[▼]-[▼]-[▼]-[▼]                    [ ▼ ]|
 |[ ▼ ][ ▼ ][ ▼ ]     [_________________▼________________]               [▼][▼][▼][To(3)]|
 |__________________________________________________________________________________________|



--Layer 4--
  _________________________________________________________________________________________
 |[Tg(4)]     [▼]-[Cs0]-[Ds0]-[▼]-[Fs0]-[Gs0]-[As0]-[▼]-[▼]-[▼]-[▼]-[▼]             [ ▼ ]|
 |[ MIDITgg ]  [C0]-[D0]-[E0]-[F0]-[G0]-[A0]-[B0]-[▼]-[▼]-[Trs+]-[▼]-[▼]           [  ▼  ]|
 |[ ▼ ]        [▼]-[Cs1]-[Ds1]-[▼]-[Fs1]-[Gs1]-[As1]-[▼]-[▼]-[▼]-[▼]              [  ▼  ]|
 |[   ▼   ]     [C1]-[D1]-[E1]-[F1]-[G1]-[A1]-[B1]-[Oct-]-[Oct+]-[Trs-]               [ ▼ ]|
 |[ ▼ ][ ▼ ][ ▼ ]     [_________________▼________________]               [▼][▼][▼][To(3)]|
 |__________________________________________________________________________________________|

 */

#include QMK_KEYBOARD_H

// Layers
#define _BASE 0
#define _UTIL 1
#define _LITE 2
#define _MISC 3
#define _MIDI 4

// Key index shortcuts
#define I_SPACEBAR 57

// Quickly send a WinCompose sequence
#define COMPOSED(s) SEND_STRING(SS_RALT(s))

// Not sure what this does but if I remove it
// my macros don't work.
enum macro_keycodes {
  M_1 = SAFE_RANGE,
  M_2,
  M_3,
  M_4,
  M_5,
  M_6,
  M_7,
  M_8,
  M_9,
  M_10,
  M_11,
  M_12
};

// Some of these names don't have extra meaning, just letters
// to make them 5 characters long. Try not to think too hard
// about what S_ or _L mean, or why "RESET" and "TERMINAL"
// are spelled that way.
enum color_map {
  S_OFF = 0,
  PAN_A,
  PAN_B,
  PAN_C,
  MAC_L,
  NPK_L, // Numpad keys
  R_SET, // Reset
  T_RML, // Terminal
  WH_VR, // Whatever
  KEY_W, // White key
  KEY_B  // Black key
};

const uint8_t colors[][3] = {
  {0,   0,   0},   // unused
  {255, 27,  141}, // PAN_A, philippine pink
  {255, 218, 0},   // PAN_B, sizzling sunrise
  {27,  179, 255}, // PAN_C, spiro disco ball
  {0,   0,   255}, // NPK_L, numpad keys
  {0,   255, 212}, // MAC_L, macros, Some(Blue)
  {255, 0,   0},   // R_SET, reset, red af
  {0,   255, 0},   // T_RML, terminal hotkey, green af
  {20,  160, 65},  // WH_VR, whatever, cyanish
  {255, 218, 0},   // KEY_W, white MIDI keyboard key
  {27,  179, 255}  // KEY_B, black MIDI keyboard key
};

const uint8_t led_layer_map[][RGB_MATRIX_LED_COUNT] = {
  [_BASE] = {
    S_OFF, S_OFF, S_OFF, S_OFF, S_OFF, S_OFF, S_OFF, S_OFF, S_OFF, S_OFF, S_OFF, S_OFF, S_OFF, S_OFF,
    S_OFF, S_OFF, S_OFF, S_OFF, S_OFF, S_OFF, S_OFF, S_OFF, S_OFF, S_OFF, S_OFF, S_OFF, S_OFF, S_OFF,
    S_OFF, S_OFF, S_OFF, S_OFF, S_OFF, S_OFF, S_OFF, S_OFF, S_OFF, S_OFF, S_OFF, S_OFF,        S_OFF,
    S_OFF,        S_OFF, S_OFF, S_OFF, S_OFF, S_OFF, S_OFF, S_OFF, S_OFF, S_OFF, S_OFF,        S_OFF,
    S_OFF, S_OFF, S_OFF,                      S_OFF,                      S_OFF, PAN_A, PAN_B, S_OFF
  },
  [_UTIL] = {
    PAN_A, PAN_A, PAN_A, PAN_A, PAN_A, PAN_A, PAN_A, PAN_A, PAN_A, PAN_A, PAN_A, PAN_A, PAN_A, PAN_A,
    S_OFF, PAN_A, PAN_A, PAN_A, PAN_A, S_OFF, PAN_A, S_OFF, PAN_A, PAN_A, PAN_A, PAN_A, S_OFF, R_SET,
    S_OFF, PAN_A, PAN_A, PAN_A, PAN_A, PAN_A, PAN_A, PAN_A, PAN_A, PAN_A, PAN_A, PAN_A,        S_OFF,
    S_OFF,        S_OFF, S_OFF, S_OFF, S_OFF, S_OFF, PAN_A, PAN_A, PAN_A, PAN_A, PAN_A,        S_OFF,
    S_OFF, S_OFF, S_OFF,                      S_OFF,                      S_OFF, S_OFF, PAN_C, S_OFF
  },
  [_LITE] = {
    S_OFF, NPK_L, NPK_L, NPK_L, NPK_L, NPK_L, NPK_L, NPK_L, NPK_L, NPK_L, NPK_L, NPK_L, NPK_L, T_RML,
    PAN_B, PAN_B, PAN_B, PAN_B, PAN_B, S_OFF, S_OFF, S_OFF, S_OFF, S_OFF, S_OFF, S_OFF, S_OFF, NPK_L,
    S_OFF, PAN_B, PAN_B, PAN_B, PAN_B, S_OFF, S_OFF, S_OFF, S_OFF, S_OFF, S_OFF, NPK_L,        NPK_L,
    S_OFF,        S_OFF, S_OFF, PAN_B, PAN_B, S_OFF, S_OFF, S_OFF, NPK_L, NPK_L, NPK_L,        S_OFF,
    S_OFF, S_OFF, S_OFF,                      S_OFF,                      S_OFF, PAN_C, S_OFF, S_OFF
  },
  [_MISC] = {
    S_OFF, MAC_L, MAC_L, MAC_L, MAC_L, MAC_L, MAC_L, MAC_L, MAC_L, MAC_L, MAC_L, MAC_L, MAC_L, MAC_L,
    S_OFF, S_OFF, S_OFF, S_OFF, S_OFF, S_OFF, S_OFF, S_OFF, S_OFF, S_OFF, S_OFF, S_OFF, S_OFF, S_OFF,
    PAN_A, S_OFF, S_OFF, S_OFF, S_OFF, S_OFF, S_OFF, S_OFF, S_OFF, S_OFF, S_OFF, S_OFF,        S_OFF,
    S_OFF,        S_OFF, S_OFF, S_OFF, S_OFF, S_OFF, S_OFF, S_OFF, S_OFF, S_OFF, S_OFF,        S_OFF,
    S_OFF, S_OFF, S_OFF,                      S_OFF,                      S_OFF, S_OFF, S_OFF, S_OFF
  },
  [_MIDI] = {
    R_SET, S_OFF, KEY_B, KEY_B, S_OFF, KEY_B, KEY_B, KEY_B, S_OFF, S_OFF, S_OFF, S_OFF, S_OFF, S_OFF,
    PAN_A, KEY_W, KEY_W, KEY_W, KEY_W, KEY_W, KEY_W, KEY_W, S_OFF, S_OFF, S_OFF, NPK_L, S_OFF, S_OFF,
    S_OFF, S_OFF, KEY_B, KEY_B, S_OFF, KEY_B, KEY_B, KEY_B, S_OFF, S_OFF, S_OFF, S_OFF,        S_OFF,
    S_OFF,        KEY_W, KEY_W, KEY_W, KEY_W, KEY_W, KEY_W, KEY_W, R_SET, NPK_L, R_SET,        S_OFF,
    S_OFF, S_OFF, S_OFF,                      S_OFF,                      S_OFF, S_OFF, S_OFF, S_OFF
  }
};

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
    [_BASE] = LAYOUT_60_ansi(
        QK_GESC, KC_1,    KC_2,    KC_3,    KC_4,    KC_5,    KC_6,    KC_7,    KC_8,    KC_9,    KC_0,    KC_MINS, KC_EQL,  KC_BSPC, \
        KC_TAB,  KC_Q,    KC_W,    KC_E,    KC_R,    KC_T,    KC_Y,    KC_U,    KC_I,    KC_O,    KC_P,    KC_LBRC, KC_RBRC, KC_BSLS, \
        MO(1),   KC_A,    KC_S,    KC_D,    KC_F,    KC_G,    KC_H,    KC_J,    KC_K,    KC_L,    KC_SCLN, KC_QUOT,          KC_ENT, \
        KC_LSFT,          KC_Z,    KC_X,    KC_C,    KC_V,    KC_B,    KC_N,    KC_M,    KC_COMM, KC_DOT,  KC_SLSH,          KC_RSFT, \
        KC_LCTL, KC_LGUI, KC_LALT,                            KC_SPC,                             KC_RALT, MO(1),   MO(2),   KC_RCTL),
    [_UTIL] = LAYOUT_60_ansi(
        KC_GRV,  KC_F1,   KC_F2,   KC_F3,   KC_F4,   KC_F5,   KC_F6,   KC_F7,   KC_F8,   KC_F9,   KC_F10,  KC_F11,  KC_F12,  KC_DEL, \
        _______, KC_MPRV, KC_MPLY, KC_MNXT, _______, _______, KC_CALC, _______, KC_PSCR, KC_INS,  KC_PAUS, KC_UP,   _______, QK_BOOT, \
        _______, KC_VOLU, KC_VOLD, KC_MUTE, KC_EJCT, _______, KC_PAST, KC_PSLS, KC_HOME, KC_PGUP, KC_LEFT, KC_RGHT,        _______, \
        _______,          _______, _______, _______, _______, _______, KC_PPLS, KC_PMNS, KC_END,  KC_PGDN, KC_DOWN,        _______, \
        _______, _______, _______,                            _______,                            _______, _______, MO(3), _______),
    [_LITE] = LAYOUT_60_ansi(
        VK_TOGG, KC_P1,   KC_P2,    KC_P3,   KC_P4,   KC_P5,   KC_P6,   KC_P7,   KC_P8,   KC_P9,   KC_P0,   KC_PMNS, KC_PPLS, RCTL(KC_GRV), \
        RGB_TOG, RGB_MOD, RGB_RMOD, RGB_HUI, RGB_HUD, _______, _______, _______, _______, _______, _______, _______, KC_PEQL, _______, \
        MO(3),   RGB_SPI, RGB_SPD,  RGB_SAI, RGB_SAD, _______, _______, _______, _______, _______, _______, KC_PAST,          KC_PENT, \
        _______,          _______, _______,  RGB_VAI, RGB_VAD, _______, _______, _______, KC_PCMM, KC_PDOT, KC_PSLS,          _______, \
        _______, _______, _______,                             _______,                              _______, MO(3),   _______, _______),
    [_MISC] = LAYOUT_60_ansi(
        _______, M_1,     M_2,     M_3,     M_4,     M_5,     M_6,     M_7,     M_8,     M_9,     M_10,    M_11,    M_12,    _______, \
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, \
        TG(4),   _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,          _______, \
        _______,          _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,          _______, \
        _______, _______, _______,                            _______,                            _______, _______, _______, _______),
    [_MIDI] = LAYOUT_60_ansi(
        TG(4),   _______, MI_Cs,   MI_Ds,   _______, MI_Fs,   MI_Gs,   MI_As,   _______, _______, _______, _______, _______, _______, \
        MI_TOGG, MI_C,    MI_D,    MI_E,    MI_F,    MI_G,    MI_A,    MI_B,    _______, _______, _______, MI_TRSU, _______, _______, \
        _______, _______, MI_Cs1,  MI_Ds1,  _______, MI_Fs1,  MI_Gs1,  MI_As1,  _______, _______, _______, _______,          _______, \
        _______,          MI_C1,   MI_D1,   MI_E1,   MI_F1,   MI_G1,   MI_A1,   MI_B1,   MI_OCTD, MI_OCTU, MI_TRSD,          _______, \
        _______, _______, _______,                            _______,                            _______, _______, _______, _______)
};

#ifdef __LOCAL_DEBUG
void debug_led_colors(void) {
  for (int c = 15, i = 0; i < 20; i++, c = c + 8) {
    rgb_matrix_set_color( i, 255, c, c );
  }

  for (int c = 15, i = 0; i < 20; i++, c = c + 8) {
    rgb_matrix_set_color( i + 20, c, 255, c );
  }

  for (int c = 15, i = 0; i <= 20; i++, c = c + 8) {
    rgb_matrix_set_color( i + 40, c, c, 255 );
  }
}
#endif

void set_leds_color(int layer) {
  /* Key traversal this way does not work the way it appears. It sweeps right to
   * left starting from the top right, meaning the color mappings for the rows
   * are mirrored. This needs to be fixed in some clever way because I don't want
   * to have to mirror the color mappings manually either at compile- or runtime.
   */
  for (uint8_t n = 0; n < RGB_MATRIX_LED_COUNT; n++) {
    uint8_t color = led_layer_map[layer][n];

    if (color != S_OFF) {
      uint8_t i;

      // This is junk but it works for now.
      if (n <= 13) {
        i = 13 - n;
      } else if (n >= 14 && n <= 27) {
        i = 41 - n;
      } else if (n >= 28 && n <= 40) {
        i = 68 - n;
      } else if (n >= 41 && n <= 52) {
        i = 93 - n;
      } else { // if (n >= 53 && n <= 60) {
        i = 113 - n;
      }
      
      rgb_matrix_set_color( i, colors[color][0], colors[color][1], colors[color][2] );
    }
  }
}

// Illuminates bound keys according to the currently active layer.
void apply_layer_lighting(void) {
  switch (biton32(layer_state)) {
    case _UTIL:
      #ifdef __LAYER_1_LIGHTING
      set_leds_color(_UTIL);
      #endif
      break;
    case _LITE:
      #ifdef __LAYER_2_LIGHTING
      set_leds_color(_LITE);
      #endif
      break;
    case _MISC:
      #ifdef __LAYER_3_LIGHTING
      set_leds_color(_MISC);
      #endif
      break;
    case _MIDI:
      #ifdef __LAYER_4_LIGHTING
      set_leds_color(_MIDI);
      #endif
      break;
    default:
      #ifdef __LOCAL_DEBUG
      debug_led_colors();
      #endif
      break;
  }
}

#ifdef __SPACEBAR_WPM
// Lights up the space bar based on the current WPM.
void apply_spacebar_wpm(void) {
  uint8_t wpm = get_current_wpm();
  // TODO: Map WPM to a color more smoothly. Also figure out how it works, exactly.
  if (wpm < 10) {
    rgb_matrix_set_color( I_SPACEBAR, 0, 0, 0 );
  } else if (wpm >= 10) {
    rgb_matrix_set_color( I_SPACEBAR, 0, 0, 255 );
  } else if (wpm >= 35) {
    rgb_matrix_set_color( I_SPACEBAR, 185, 0, 204 );
  } else if (wpm >= 70) {
    rgb_matrix_set_color( I_SPACEBAR, 250, 0, 110 );
  } else if (wpm >= 100) {
    rgb_matrix_set_color( I_SPACEBAR, 255, 0, 0 );
  }
}
#endif

// Apply all illumination.
bool rgb_matrix_indicators_user(void) {
  // Don't apply to the base layer, let the keyboard do that one.
  if (biton32(layer_state) != _BASE) {
    apply_layer_lighting();

    #ifdef __SPACEBAR_WPM
    apply_spacebar_wpm();
    #endif
    
    return true;
  }

  return false;
}

// Macros
bool process_record_user(uint16_t keycode, keyrecord_t *record) {
  switch (keycode) {
    case M_1:
      if (record->event.pressed) {
        COMPOSED("lf");
      }
      return false;
    case M_2:
      if (record->event.pressed) {
        COMPOSED("sf");
      }
      return false;
    case M_3:
      if (record->event.pressed) {
        COMPOSED("rsf");
      }
      return false;
    case M_4:
      if (record->event.pressed) {} else {}
      break;
    case M_5:
      if (record->event.pressed) {} else {}
      break;
    case M_6:
      if (record->event.pressed) {} else {}
      break;
    case M_7:
      if (record->event.pressed) {} else {}
      break;
    case M_8:
      if (record->event.pressed) {} else {}
      break;
    case M_9:
      if (record->event.pressed) {} else {}
      break;
    case M_10:
      if (record->event.pressed) {} else {}
      break;
    case M_11:
      if (record->event.pressed) {} else {}
      break;
    case M_12:
      if (record->event.pressed) {} else {}
      break;
    default:
      break;
  }
  return true;
}
