# sli60

My personal keyboard layout. Build with:

```bash
$ qmk compile -kb dztech/dz60rgb_ansi/v1 -km sli60
```

### Features

* Lightable bindings
* Five total layers
* MIDI controls

### Random Notes

* Spacebar = 57